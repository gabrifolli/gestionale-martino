import projectSchema from './Project.js'
import groupSchema from './Group.js'
import fileSchema from './File.js'
import userSchema from './User.js'

let schemas = {
  projectSchema,
  groupSchema,
  fileSchema,
  userSchema
}

export default schemas