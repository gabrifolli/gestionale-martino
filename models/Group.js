import mongoose from 'mongoose';
import { groupId } from '../lib/ids.js'
import { getMultiConnection } from '../lib/connections.js';

const groupSchema = new mongoose.Schema(
  {
    _id: { type: String, default: groupId },
    project_id: { type: String, index: true },
    type: { type: String },
    name: { type: String, required: true },
    description: { type: String },
    // codice del gruppo, i vari gruppi sono salvati nel db libreria
    ref: {
      symbol: { type: String, index: true },
      next_val: { type: Number }
    },
    // file del gruppo
    files: [{ type: String, ref: 'Files', index: true }],
    indexed: { type: Boolean, required: true, default: false }
  }
)

groupSchema.pre('save', async function (next) {
  if (this.isNew) {
  let db = getMultiConnection()
  let Project = db.model('Projects')

  let _project = await Project.findById(this.project_id).exec()
  _project.groups.push(this.id)

  await _project.save()
  next()
  } else {
    next()
  }
})

export default groupSchema