import mongoose from "mongoose";
import { projectId } from "../lib/ids.js";

const schemaOpts = {
  timestamps: {
    createdAt: "created_at",
    updatedAt: "updated_at",
  },
  minimize: false 
}

const pointSchema = new mongoose.Schema(
  {
  type: { type: String, enum: ["Point"], required: true },
  coordinates: { type: [Number], required: true },
  }
);

const projectSchema = new mongoose.Schema(
  {
    _id: { type: String, default: projectId },
    _index_id: { type: String },
    name: { type: String, required: true },
    description: { type: String, default: null },
    kind: { type: [String], required: true },
    ref: {
      phase: { type: Number, default: null },
      group_tag: { type: String, default: null },
      seq: { type: Number, default: null },
    },
    status: { type: String, enum: ["open", "closed", "deleted"], default: 'open' },
    data: {
      address: {
        country: { type: String, default: null, index: true },
        city: { type: String, default: null, index: true },
        line1: { type: String, default: null },
        line2: { type: String, default: null },
        province: { type: String, default: null, index: true },
        postal_code: { type: Number, default: null, index: true },
        geo: { type: pointSchema, index: "2dsphere" },
      },
      customers: { type: [String] },
      contractors: { type: [String] },
      conversations: { type: Array },
    },
    groups: [{ type: String, ref: 'Groups', index: true }],
    owner: { type: [Object], required: true },
    permissions: { type: [Object] },
    assigned_to: [{ type: String, ref: 'Users', index: true}],
    checklist: { type: [Object] }

  }, schemaOpts);

export default projectSchema;
