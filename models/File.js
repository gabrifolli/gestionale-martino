import mongoose from 'mongoose';

import { fileId } from '../lib/ids.js'

const schemaOpts = {
  timestamps: {
    createdAt: "created_at",
    updatedAt: "updated_at",
  },
  minimize: false 

}

const fileSchema = new mongoose.Schema({
  _id: {
    type: String,
    required: true,
    default: fileId
  },
  project_id: {
    type: String
  },
  group_id: {
    type: String, 
    ref: 'Groups',
    index: true
  },
  name: String,
  description: String,
  type: {
    type: String,
    index: true,
  },
  extention: {
    type: String,
    index: true
  },
  is_open: { type: Boolean, default: false},
  ref: {
    index: { type: String, index: true },
    number: []
  },
  checklist: {
    type: [Object]
  },
  
}, schemaOpts)


export default fileSchema