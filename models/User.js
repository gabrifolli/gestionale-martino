import mongoose from "mongoose";
import bcrypt from "bcrypt";
import { userId } from '../lib/ids.js'
import { getProjectConnection } from '../lib/connections.js'

const schemaOpts = {
  timestamps: {
    createdAt: "created_at",
    updatedAt: "updated_at",
  },
  minimize: false 

}

const userSchema = new mongoose.Schema({
  _id: { type: String, required: true, default: userId },
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  token: { type: String },
  password: { type: String, required: true },
  assigned_to: [{ type: String, ref: 'Project', index: true}],
  
}, schemaOpts )

userSchema.methods.comparePassword = async function (password) {
  return await bcrypt.compare(password, this.password)
}

userSchema.pre('save', async function(next) {
  if (!this.isModified('password')) return next();
  try {
    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt)
    return next();
  } catch (e) {
    return next(e)
  } 
})

userSchema.pre('remove', { document: true, query: false }, async function(next) {
  let db = getProjectConnection()
  let Projects = db.model('Projects')

  try {
    let projects = await Projects.find({ assigned_to: this.id})
    if (projects.length === 0) next();

    for (let project of projects) {
      project.assigned_to.pop()
      await project.save()
    }
    next()
  } catch (e) {
    throw e
  }
})


export default userSchema 
