import * as dotenv from 'dotenv';
dotenv.config()

import mongoose from 'mongoose'
import schemaRepo from '../models/repo.js'

const { 
  projectSchema, 
  groupSchema, 
  fileSchema, 
  userSchema 
} = schemaRepo;

const DB_URI = process.env.DB_URI
const DB_NAME = process.env.DB_NAME
const DB_USER = process.env.DB_USER
const DB_PASS = process.env.DB_PASS

const options = {
  authSource: 'admin',
  user: DB_USER,
  pass: DB_PASS,
  maxPoolSize: 30
}

export const getMultiConnection = () => {
  let conn = mongoose.createConnection(`${DB_URI}/${DB_NAME}`, options)

  conn.model('Projects', projectSchema )
  conn.model('Groups', groupSchema)
  conn.model('Files', fileSchema)
  conn.model('Users', userSchema)

  return conn
}

export const getFileConnection = () => {
  let conn = mongoose.createConnection(`${DB_URI}/${DB_NAME}`, options)

  conn.model('File', fileSchema)

  return conn
}

export const getProjectConnection = () => {
  let conn = mongoose.createConnection(`${DB_URI}/${DB_NAME}`, options)

  conn.model('Projects', projectSchema )
  
  return conn
}

export const getGroupConnection = () => {
  let conn = mongoose.createConnection(`${DB_URI}/${DB_NAME}`, options)

  conn.model('Groups', projectSchema )
  
  return conn
}

export const getOnlyConnection = () => {
  let conn = mongoose.createConnection(`${DB_URI}/${DB_NAME}`, options)

  return conn
}