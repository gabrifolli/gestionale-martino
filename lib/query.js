export default class Query { 
   Users = (db) => {
    return {
    newUser: async function (query) {
      const User = db.model("Users");

      try {
        let usr = new User(query);

        await usr.save();
        return usr;
      } catch (e) {
        if (e.code === 11000)
          throw { error: `${e.keyValue.email} already exists` };
        throw e;
      }
    },
    updateUser: async function (query) {
      let User = db.model("Users");

      if (!query.id)
        throw {
          error: "an user id is required",
        };

      if (!query.update)
        throw {
          error: "update prop not set, pass an object with the updates",
        };

      try {
        let user = await User.updateOne({ _id: query.id }, query.update);

        if (!user.acknowledged) throw "can't update";

        return { ok: 1 };
      } catch (e) {
        throw {
          error: e,
        };
      }
    },
    deleteUser: async function (query) {
      let User = db.model("Users");

      try {
        let usr = await User.findById(query.id);

        if (!usr) throw "user not found";

        await usr.remove();

        return { ok: "bye" };
      } catch (e) {
        throw {
          error: e,
        };
      }
    },
    changeUserPass: async function (query) {
      let User = db.model("Users");

      try {
        let user = await User.findById(query.id);

        if (!user) throw "user not found";

        let result = await user.comparePassword(query.oldPass);
        if (!result) throw "passwords mismatch";

        user.password = query.newPass;

        await user.save();
        return { ok: 1 };
      } catch (e) {
        throw {
          error: e,
        };
      }
    },
    addUserToProject: async function (query) {
      const Project = db.model("Projects");
      const User = db.model("Users");

      let user_id = query.props.user_id;
      let project_id = query.id

      if (!user_id) throw "pass user_id in query.props with an user id";
      if (!project_id) throw "pass id to query with project id";

      try {
        // cerco il progetto e l'utente per id
        const proj = await Project.findById(project_id);
        const usr = await User.findById(user_id);

        //controllo se l'array di assigned_to in project è vuoto
        if (proj.assigned_to.length === 0) {
          // se sì aggiungo direttamente utente a progetto e viceversa e salvo
          proj.assigned_to.push(user_id);
          usr.assigned_to.push(project_id);

          await proj.save();
          await usr.save();

          return { ok: 1 };
        }
        // altrimenti controllo se l'utente è già presente nel progetto
        if (proj.assigned_to.includes(user_id))
          throw "user already assigned to project";

        // passati tutti i controlli salvo l'utente nel progetto e viceversa
        proj.assigned_to.push(user_id);
        usr.assigned_to.push(project_id);

        await proj.save();
        await usr.save();

        return { ok: 1 };
      } catch (e) {
        throw {
          error: e
        };
      }
    },
    removeUserFromProject: async function (query) {
      const Project = db.model("Projects");
      const User = db.model("Users");

      let user_id = query.props.user_id;
      let project_id = query.id

      if (!user_id) throw "pass user_id in query.props with an user id";
      if (!project_id) throw "pass id to query with project id";

      try {
        let proj = await Project.findById(project_id);
        let usr = await User.findById(user_id);

        if (proj.assigned_to.length === 0)
          throw "No users assigned to this project yet";

        if (proj.assigned_to.includes(user_id)) {
          proj.assigned_to.pull(user_id);
          usr.assigned_to.pull(project_id);

          await usr.save();
          await proj.save();

          return { ok: 1 };
        }

        throw "Cannot remove! User not assigned to this project";
      } catch (e) {
        throw { error: e };
      }
    }
  }}

  Project = (db) => {
    return {
    newProject: async function (query) {
      if (!query.props) throw 'pass a project config object to props option'
      try {
        let Project = db.model("Projects");
        let proj = new Project(query.props);
  
        await proj.save();
  
        return proj;
      } catch (e) {
        throw {
          error: e 
        };
      }
    },
    getProjects: async function () {
      let Project = db.model('Projects');

      try {
        let projects = await Project.find({})
        if (!projects) 
          throw 'projects not found';

        return projects;
      } catch (e) {
        throw {
          error: e
        }
      }
    },
    getProjectsByTag: async function (query) {
      let Project = db.model("Projects");
    
      if (!query.tag) throw "pass a tag or an array of tags to props";
      
      if(Array.isArray(query.tag)) {
        console.log('array')
        try {
          let projects = await Project.find({ 'ref.group_tag': { $in: query.tag }}).sort({'ref.group_tag': 1})
          if (!projects.length === 0) 
            throw "projects not found";

          return projects
        } catch (e) {
          throw e
        }
      }
    },
    getProjectById: async function (query) {
      let Project = db.model("Projects");

      if (!query.id) throw "a project id is required";

      try {
        let proj = await Project.findById(query.id);
        if (!proj) 
          throw 'project not found';

        return proj
      } catch (e) {
        throw e;
      }
    },
    updateProjectInfo: async function (query) {
      let Project = db.model("Projects");

      if (!query.id) throw "id option is required";
      if (!query.update) throw "please specify an update object";

      try {
        let proj = Project.updateOne({ _id: query.id }, query.update);

        if (!proj) throw "project not found";
        if (!proj.acknowledged) throw "can't update";

        return { ok: 1 };
      } catch (e) {
        throw {
          error: e
        }
      }
    },
    deleteProject: async function (query) {
      let Project = db.model("Projects");

      try {
        let proj = await Project.findById(query.id);

        if (!proj) throw 'project not found';

        await proj.remove();
        return { ok: 1 }
      } catch (e) {
        throw {
          error: e
        }
      }
    }
  }}
}


export const newGroup = (db) =>
  async function (query) {
    if (!query.id) throw 'id option with project id is required'
    if (!query.props) throw 'pass a group config object to props option'
    
    const Group = db.model("Groups");
    let grp = new Group(query.props);

    grp.project_id = query.id;

    try {
      await grp.save();
      return grp
    } catch (e) {
      throw e;
    }
  };

export const newFile = (db) =>
  async function (query) {
    if (!query.id) throw "a group id is required";

    if (!query.props) throw 'props value not set, please pass a config object'

    const File = db.model("Files");
    const Group = db.model("Groups");

    let file = new File(query);
    let grp = await Group.findById(query.id);

    grp.files.push(file.id);

    file.group_id = query.id;

    try {
      await file.save();
      await grp.save();
      return file
    } catch (e) {
      throw e;
    }
  };
