import crypto from 'crypto';

const idLength = 10
// base ID 
export function genId() {
  return crypto.randomBytes(16).toString('hex').slice(0, idLength)
}
// UserID
export function userId() {
  return 'usr-' + genId();
}
// file ID
export function fileId() {
  return 'fs-' + genId();
}
// library Id
export function libraryId() {
  return 'lib-' + genId();
}
// opration ID
export function opId() {
  return 'op-' + genId();
}

export function folderId() {
  return 'fol-' + genId();
}

export function projectId() {
  return 'proj-' + genId();
}

export function seqId() {
  return 'seq-' + genId();
}

export function groupId() {
  return 'grp-' + genId();
}