import fastify from 'fastify';

const app = fastify({logger: true})

app.get('/', (req, res) => {
  return { hello: 'MongoDB query service is ready!'}
})

try {
  await app.listen({port: 8080})
} catch (e) {
  app.log.error(e)
}