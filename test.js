import mongoose from "mongoose";
import * as xml2js from 'xml2js'
import fs from 'fs-extra'
import util from 'util'
import { normalize, firstCharLowerCase, parseNumbers, parseBooleans } from 'xml2js/lib/processors.js'

import { getMultiConnection } from './lib/connections.js'
import { 
  newProject, 
  newGroup, 
  newFile, 
  newUser,
  updateUser,
  addUserToProject,
  removeUserFromProject, 
  deleteUser,
  changeUserPass,
  compareUserPass,
  getProjectByTag
} from "./lib/queries.js";

import { genId } from './lib/ids.js'

let db = getMultiConnection()

// try {
//   let result = await updateUser(db)({ user_id: 'usr-2b23210ab8', update: { name: 'Gabriele' , last: 'Folli'}})
//   console.log(result)
// } catch (e) {
//   console.log(e)
// }


let projectOptions = {
  name: 'Test Project',
  kind: ['waste-to-energy'],
  owner: [{ name: 'gabriele'}]
}

let groupOptions = {
  name: 'Test File',
  ref: {
    symbol: 'ITV'
  }
}

let userOpts = {
  name: 'gabriele',
  last: 'folli',
  email: `${genId()}@gmail.com`,
  password: 'Musicplanet95' 
}

// try {
//   let project = await newProject(db)(projectOptions)
//   console.log(project)
// } catch (e) {
//   console.log(e)
// }

// newProject(db)({props: projectOptions})
//   .then(async (project) => {
//     let group = newGroup(db)({id: project.id, props: groupOptions})
//     console.log(project);
//     return group
//   })
//   .then((grp) => console.log(grp))
//   .catch((err) => {
//     console.log(err);
// });

// try {
//   let group = await newGroup(db)('proj-a25b57b699', groupOptions)
//   console.log(group)
// } catch (e) {
//   console.log(e)
// }

// try {
//   let file = await newFile(db)('grp-c41af63998', {name: 'Test File'})
//   console.log(file)
// } catch (e) {
//   console.log(e)
// }

// let File = db.model('Files')

// File
//   .findOne({ _id: 'fs-e2cf225fdb'})
//   .populate('group_id')
//   .exec().then((file) => console.log(file))
//   .catch((error) => console.log(error))

// try {
//   let user = await newUser(db)(userOpts)
//   console.log(user)
// } catch (e) {
//   console.error(e)
// }

// try {
//   let result = await addUserToProject(db)('usr-2b23210ab8', 'proj-7177fbca6d')
//   console.log(result)
// } catch (e) {
//   console.log(e)
// }

// try {
//   let result = await removeUserFromProject(db)('usr-c9e0090c9', 'proj-7177fbca6d')
//   console.log(result)
// } catch (e) {
//   console.log(e)
// }

// try {
//   let result = await compareUserPass(db)('usr-21c3f0c87a', 'Prova123')
//   console.log(result)
// } catch (e) {
//   console.log(e)
// }

// const parserOpts = {
//   tagNameProcessors: [firstCharLowerCase, parseNumbers],
//   attrNameProcessors: [firstCharLowerCase, parseNumbers],
//   attrValueProcessors: [normalize, parseNumbers, parseBooleans],
//   emptyTag: () => ({}),
//   explicitChildren: false,
//   explicitArray: true,

// }

// const parser = new xml2js.Parser(parserOpts)
// const data = await fs.readFile('./family.xml', 'utf-16le')

// parser.parseStringPromise(data).then((result) => {

//   const data = changeTags(result)
//   console.log(data.family.familyParameters)
// }).catch(e => console.log(e))


// function changeTags(_obj) {
//   let isValidObject = (object) => typeof obj === 'object' && object !== null;
//   let obj = _obj

//   if (isValidObject(_obj)) {
//     obj = {..._obj}
 
//     for (let key in obj) {
      
//       if (key === '$') {
//         obj._TAGS = obj[key]
//         delete obj[key]
//       }

//       if (isValidObject(obj[key])) {
//         obj[key] = changeTags(obj[key])
//       }
//     }
//   }
//   return obj
// }

try {
  let results = await getProjectByTag(db)({tag: ['IRR', 'ITV']})
  console.log(results)
} catch (e) {
  console.log(e)
}