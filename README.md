# Database

Il servizio di seguito descritto è costituito
da un un webserver implementato con il framework
[Fastify](https://github.com/fastify/fastify)
dove è implementata un'interfaccia di tipo REST per
gli insert e le query al database, implementato con [MongoDb](https://www.mongodb.com/docs/) e
[Mongoose](https://mongoosejs.com/docs/) come libreria **ODM** (Object Data Modeling).

<details>

<summary><b>Roadmap</b></summary>

Qui sono riportate tutte le funzioni da aggiungere e i bug
da risolvere

## Aggiungere

funzioni da aggiungere per queries o nuovi modelli da
implementare

- [ ] Inserire GridFS come FileSystem

- [ ] Progressivi - Files e Progetti
- [x] modello ***user*** - Utente piattaforma
- [ ] modello ***client*** - Committente
- [ ] modello ***contractor*** - Costruttore
- [ ] modello ***checklist*** - Task da completare
- [ ] modello ***listOfPapers*** - Elenco elaborati
- [ ] modello ***Kind*** - Tipo di progetto
- [ ] gestione versioning dei documenti
- [ ] implementare database **Libreria**
- [ ] modello indici di progetto

## Modificare

- [ ] modello ***operation*** - Cronologia delle operazioni per [History](#history-model)
- [ ] Modificare struttura Project in descrizione
- [ ] aggiungere al modello Files la proprietà `tags`

## Rimuovere

## Changes

Modifiche e release maggiori alla repo

### commit `database/#c41bbc5`

- Creato file `lib/connection.js` con esportati vari metodi che ritornano connessioni con registrati uno o più modelli in base al tipo di query da eseguire.

  Metodi disponibili al tempo della commit:

  - `getMultiConnection()`  -  ritorna una connessione di mongoose al database con registrati i modelli di `Projects`, `Groups` e `Files`

  - `getProjectConnection` - ritorna una connessione di mongoose al database con registrato solo il modello `Projects`

  - `getGroupConnection` - ritorna una connessione di mongoose al database con registrato solo il modello `Group`

  - `getFileConnection` - ritorna una connessione di mongoose al database con registrato solo il modello `File`
  
- modificato file `lib/db.js` e rinominato in `lib/queries` contenente i metodi per effettuare le queries al database.

  Metodi implementati al momento della commit:
  
  - `newProject()` - Crea un nuovo progetto e ritorna il suo id
  - `newGroup()` - Crea un nuovo gruppo all'interno di un progetto e ritorna il suo id
  - `newFile()` - Crea un nuovo file, lo salva all'interno di un gruppo e ritorna il suo id

- cretao file `schema/repo.js` con esportati in un'oggetto tutti gli schemi del database

</details>

## Introduzione

La prima parte dello scheletro che compone il database è formata dai Progetti.

Essi contengono tutti file e le informazioni che normalmente servono per redarne uno e la logica per mantenerlo organizzato ed indicizzato.

I progetti vengono suddivisi e numerati in base alla loro tipologia. Ogni volta che un progetto viene creato bisogna specificare a quale tipologia di progetto appartiene per assegnarli il progressivo corretto.

Gli indici sono oggetti personalizzabili composti da blocchi di vario tipo che ne compongono la sua struttura. Vengono spiegati nel dettaglio [qua]() e se ne consiglia la lettura prima di proseguire.

### **Esempio**

assumendo che abbiamo un tipo di progetto creato e di consegnuenza il suo indice inizializzato, importiamo tutto il necessario e creiamo un nuovo progetto:

```js
// query helper method for project creation 
import { newProject } from '../lib/queries.js'
// import a mongoose connection method
import { getProjectConnection } from '../lib/connections.js'

let db = getProjectConnection(); // create a connection

const project = await newProject(db)(options) // { ok: projectId }
```

### **Nel dettaglio**

#### *Query Helper*

```js
import { newProject } from '../lib/queries.js' // query helper
//...
```

La libreria `queries.js` esporta vari metodi utili per eseguire operazioni CRUD sul database già preimpostate.

Dato che dobbiamo crare un nuovo progetto importiamo il metodo `newProject` appunto, per creare un nuovo progetto.

#### *Connessione al database*

```js
//...
import { getProjectConnection } from '../lib/connections.js'
//...
```

Il metodo getProjectConnection ritorna una nuova connessione mongoose al database registrando già sulla stessa il modello `'Projects'`.

Ci sono diversi metodi per creare una connessione, [qui](/lib/CONNECTIONS.md) sono elencati tutti.

#### *Creiamo il progetto*

```js
//...

let db = getProjectConnection() // create a connection 
let project = await newProject(db)(options) // { ok: 'proj-a25b57b699' }
```

Ogni query helper ha bisogno di una connessione come argomento della prima funzione ed è per questo che ne dobbiamo creare una prima.

la seconda funzione accetta un'oggetto di configurazione `options` contenente i parametri richiesti e opzionali per la creazione di un progetto.

Il tutto ritorna una promise che restituisce l'id del progetto in caso risolvesse e un'oggetto errore in caso fosse rigettata.

qui tutti i parametri richiesti e opzionali dell'oggetto `options`

## `Project` - Mongoose Model

L'oggetto project, nella suo interezza, è un'oggetto che contine i riferimenti di tutti i documenti al suo interno.

Viene suddiviso tramite altri oggetti, chiamati gruppi o `groups`, che a loro volta contengo i riferimenti ai file associati a quel determinato gruppo.

Per approfondire su [gruppi](#groups) e [files](#files)

```js
/******************
 * Project Model *
*******************/

{
  _id: 'prj-382dh9dj',
  _index_id: 'indx-987d9sh0',
  name: 'Polo impiantistico Scarlino',
  description: 'Realizzazione di un nuovo polo imp...',
  kind: ['waste-to-energy'],
  ref: {
    phase: 1,
    type_tag: 'IRR',
    seq: 361
  },
  status: 'open',
  data: {
    location: {
      address: {
        city: 'Scarlino',
        locality: 'Casone',
        line1: 'Strada Provinciale Scarlino',
        line2: null,
        province: 'GR',
        postal_code: 58024,
        country: 'IT'
      },
      geo: {
        type: 'Point',
        coordinates: [45.35545, 12.25454] // [Lon, Lat]
      }
    },
    customers: ['cus-3984jl39', 'cus-354vv982'],
    contractors: ['cont-22lknd90'],
    conversations: [message]
  },
  groups: ['grp-c41af63998', ...],
  owner: ['usr-2937hf87'],
  permissions: [{ permission Object }],
  assigned_to: ['usr-2937hf87', 'usr-2dl3494n'],
  checklist: [{ checklist Object }],
  history: [{ history Object }],
  createdAt: Date,
  updatedAt: Date
}
```

## `Project` properties

### `Project._id` | `Project.id` - *String*

> :warning:
>impostato automaticamente alla creazione del progetto.

id del progetto

`Project.id` e `Project._id` hanno lo stesso risultato, `*.id` è un getter speciale messo a disposizione da mongoose

---

### `Project._index_id` - String

> :warning:
>impostato automaticamente alla creazione del progetto dopo l'assegnazione ad un indice.

ID dell'indice a cui il progetto è registrato

---

### `Project.description` - String

Proprietà che contiene la descrizione del progetto

---

### `Project.kind` - Array[*String*]

> :warning:
>impostato automaticamente alla creazione del progetto dopo l'assegnazione ad un indice.

Array contenente la tipologia del progetto, eventuali spazi bianchi nella stringa del tipo di progetto verranno rimpiazzati con '-'.

Es: 'waste to energy○' --> 'waste-to-energy'.

---

### `Project.ref` - Object

> :warning:
>configurato automaticamente alla creazione del progetto dopo l'assegnazione ad un indice.

`ref` contiene i riferimenti per costruire la radice della numerazione dei gruppi e dei file. Contine 3 proprietà:

#### `ref.phase` - Number

contiene l'id della fase di progettazione corrispondente nell'indice di progetto.

#### `ref.type_tag` - String

Tag associato alla tipologia a cui il progetto fà parte

Es: 'IRR'

#### `ref.seq` - Number

Progressivo assegnato dall'indice al momento della creazione

### `Project.status` - enum[*String*]

stato del progetto. Può essere ***'open'***, ***'closed'*** o ***'deleted'***

---

### `Project.data` - Object

l'oggetto `data` contiente le informazioni legate al progetto come la sua posizione, il cliente, le comunicazioni in entrate ed uscita ecc.

```js
//...
data: {
    location: {
      address: {
        city: 'Scarlino',
        locality: 'Casone',
        line1: 'Strada Provinciale Scarlino',
        line2: null,
        province: 'GR',
        postal_code: 58024,
        country: 'IT' 
      },
      geo: {
        type: 'Point',
        coordinates: [45.35545, 12.25454] // [Lon, Lat]
      }
    },
    customers: Array,
    contractors: Array,
    conversations: Array
  },
  //... others properties
```

#### `project.data.location` - Object

l'oggetto location contiene le informazioni sulla posizione geografica del progetto e le sue relative coordinate.

Contiene due proprietà; `address`, un'oggetto che contiene le informazioni sull'ubicazione del progetto e `geo`, che contiene le coordinate geografiche in latitudine e longitudine del luogo in `address`.

`address` - Object:

```js
//...
    location: { 
      address: {
        city: 'Scarlino',
        locality: 'Casone',
        line1: 'Strada Provinciale Scarlino',
        line2: null,
        province: 'GR',
        postal_code: 58024,
        country: 'IT' 
      },
      //... 
    },
//... 
```

`geo` - Object:

geo contiene le coordinate geografiche dove il progetto è localizzato.

```js
      // ...
      geo: {
        type: 'Point',
        coordinates: [45.35545, 12.25454]  // [Longitude, Latitude]
      }
      // ...
```

il parametro `type` di `geo`, dato che stiamo rappresentando un punto nello spazio, deve essere `'Point'`. Un valore diverso restituirà un errore di tipo CAST ERROR.

`coordinates` invece contine un'array di 2 valori che corrispondono alla longitudine e latitudine, rispettivamente, del luogo. Notare come la longitudine viene prima, questo perchè l'array delle coordinate in GeoJSON è rappresentato così.

#### `Project.data.customers` - Array[*String*] [Customers Model]

customers è un array contenente gli id in formato *String* del/dei cliente/i associati al progetto.

#### `Project.data.contractors` - Array[*String*] [Contractors Model]

contractors è un array contenente gli id in formato *String* della o delle ditte appaltatrici associati al progetto.

#### `Project.data.conversations` - Array[*String*] [Conversations Model]

Array contenente gli id delle conversazioni inerenti al progetto.

---

### `Project.groups` - Array[*String*] [Groups Model]

Contiene tutti gli id dei gruppi assegnati al progetto

---

### `Project.owner` - Array[*String*] [User Model]

> :warning:
>impostato automaticamente alla creazione del progetto e non può essere modificato.

array contenente l'id dell'utente che ha creato il progetto

---

### `Project.permissions` - Array[*String*] [Permissions Model]

array contenente l'id delle policy di sicurezza associate al progetto

---

### `Project.assigned_to` - Array[*String*] [Users Model]

array contenente l'id degli utenti assegnati al progetto

---

### `Project.checklist` - Array[*String*] [Checklist Model]

array contenente l'id della checklist associata al progetto.

---

### `Project.history` - Array[*String*] [History Model]

history contiene l'id di riferimento alle operazioni effettuate sul progetto.

Dalla creazione alla cancellazione ogni operazione viene registrata in history per tenere traccia dei cambiamenti e gestire le versioni del progetto.

---

### `Project.created_at` - Date

Oggetto date con il timestamp alla creazione del file

---

### `Project.updated_at` - Date

Oggetto date con il timestamp dell'ultimo aggiornamento al documento

---
